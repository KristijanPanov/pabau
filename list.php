<?php
session_start();

if (!isset($_SESSION['username'])) {
    header("Location: login.php");
    die();
}

require_once __DIR__ . "/db.php";
require_once __DIR__ . "/functions.php";


if (isset($_GET['search-term']) && !empty($_GET['search-term'])) {
    $searchTerm = strtolower($_GET['search-term']);
    $sql = "SELECT *
            FROM users
            WHERE 
                LOWER(users.name) LIKE :searchterm
                OR LOWER(email) LIKE :searchterm
                OR LOWER(phone) LIKE :searchterm
                OR LOWER(adress) LIKE :searchterm
                OR LOWER(medical_condition) LIKE :searchterm
                OR LOWER(blood_type) LIKE :searchterm 
                OR LOWER(id) LIKE :searchterm
                ";
    $stmt = $pdo->prepare($sql);
    $stmt->execute(['searchterm' => "%$searchTerm%"]);
} else {
    $sql = "SELECT *
            FROM users";
    $stmt = $pdo->query($sql);
}
?>

<?php require_once __DIR__ . "/layout/header.php"; ?>


<br />
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Email</th>
                        <th scope="col">Phone</th>
                        <th scope="col">Adress</th>
                        <th scope="col">Medical Condition</th>
                        <th scope="col">Blood Type</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if ($stmt->rowCount() == 0) {
                        echo "<tr><td colspan='8'>No users yet</td></tr>";
                    } else {
                        $count = 1;
                        while ($user = $stmt->fetch()) {
                            $encId = urlencode(encrypt($user['id']));

                            echo "<tr>
                                    <td>{$user['id']}</td>
                                    <td>{$user['name']}</td>
                                    <td>{$user['email']}</td>
                                    <td>{$user['phone']}</td>
                                    <td>{$user['adress']}</td>
                                    <td>{$user['medical_condition']}</td>
                                    <td>{$user['blood_type']}</td>
                                    <td>
                                        <a href='delete.php?id=$encId' class='btn btn-danger'>Delete</a>
                                        <a href='edit.php?id=$encId' class='btn btn-warning'>Edit</a>
                                    </td>
                                </tr>";

                            $count++;
                        }
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>


<?php require_once __DIR__ . "/layout/footer.php"; ?>