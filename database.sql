CREATE TABLE users (
    id INT UNSIGNED AUTO_INCREMENT,
    name VARCHAR(32),
    email VARCHAR(64),
    phone VARCHAR(64),
    adress VARCHAR(64),
    medical_condition VARCHAR(64),
    blood_type VARCHAR(64),
    CONSTRAINT PRIMARY KEY(id)
);
CREATE TABLE admins (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    username VARCHAR(32),
    email VARCHAR(32),
    password VARCHAR(256)
);
INSERT INTO `admins` (`username`, `email`, `password`) VALUES
('doctor@pabau.com', 'doctor@pabau.com', '$2y$10$VR8LTnPQtPp99ZZkkmMHMuKcFEN3xU6yAE5HORnoYhF6EoOf65oFW');