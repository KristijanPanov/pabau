<?php
session_start();

if (!isset($_SESSION['username'])) {
    header("Location: login.php");
    die();
}
require_once __DIR__ . "/functions.php";
require_once __DIR__ . "/db.php";

$id = decrypt($_POST['id']);
$name = $_POST['name'];
$email = $_POST['email'];
$phone = $_POST['phone'];
$adress = $_POST['adress'];
$medical_condition = $_POST['medical_condition'];
$blood_type = $_POST['blood_type'];

$sql = "UPDATE users SET 
    name = :name,
    email = :email,
    phone = :phone,
    adress = :adress,
    medical_condition = :medical_condition,
    blood_type = :blood_type
    WHERE id = :id
    ";

$stmt = $pdo->prepare($sql);
$stmt->bindValue('name', $name);
$stmt->bindValue('email', $email);
$stmt->bindValue('phone', $phone);
$stmt->bindValue('adress', $adress);
$stmt->bindValue('medical_condition', $medical_condition);
$stmt->bindValue('blood_type', $blood_type);
$stmt->bindValue('id', $id, PDO::PARAM_INT);

if ($stmt->execute()) {
    header("Location: list.php");
    die();
}

echo "Can not update at this moment";
