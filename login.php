<?php
session_start();
$message = ['error' => '', 'success' => ''];
if (isset($_SESSION['username'])) {
    //already logged in
    header("Location: index.php");
    die();
}

if ($_SERVER['REQUEST_METHOD'] == "POST") {

    require_once 'db.php';

    $sql = "SELECT * FROM admins WHERE (email = :email OR username = :username)";
    $stmt = $pdo->prepare($sql);
    $stmt->execute(['email' => $_POST['email'], 'username' => $_POST['email']]);
    if ($stmt->rowCount() == 1) {
        $admin = $stmt->fetch();
        if (password_verify($_POST['password'], $admin['password'])) {
            $_SESSION['username'] = $admin['username'];
            header("Location: index.php");
            die();
        } else {
            $message['error'] = "Password incorrect";
        }
    } else {
        $message['error'] = "Wrong credentials";
    }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin Panel</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <style>
        table,
        .bordered {
            border-bottom: 2px solid black;
        }
    </style>
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light justify-content-between">
        <a class="navbar-brand" href="index.php">Clinical Menager</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
    </nav>
    <div class="container-fluid">
        <div class="row">
            <div class="col-4 offset-4 mt-4">
                <?php
                if (!empty($message['error'])) {
                    echo "<div class='row'><div class='col-12'><div class='alert alert-danger'>{$message['error']}</div></div></div>";
                } else if (!empty($message['success'])) {
                    echo "<div class='row'><div class='col-12'><div class='alert alert-success'>{$message['success']}</div></div></div>";
                }
                ?>

                <form method="POST">
                    <div class="form-group">
                        <label for="email">Email address / username</label>
                        <input type="text" name="email" class="form-control" id="email" placeholder="Enter email or username">
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" name="password" class="form-control" id="password" placeholder="Password">
                    </div>
                    <button type="submit" class="btn btn-primary">Login</button>
                </form>
            </div>
        </div>
    </div>
</body>

</html>