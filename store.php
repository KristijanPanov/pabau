<?php
session_start();

if (!isset($_SESSION['username'])) {
    header("Location: login.php");
    die();
}
require_once __DIR__ . '/db.php';
require_once __DIR__ . '/functions.php';
// var_dump($_POST);
// die();

$name = $_POST['name'];
$email = $_POST['email'];
$phone = $_POST['phone'];
$adress = $_POST['adress'];
$medical_condition = $_POST['medical_condition'];
$blood_type = $_POST['blood_type'];

$sql = "INSERT INTO users(name, email, phone, adress, medical_condition, blood_type) 
        VALUES (:name, :email, :phone, :adress, :medical_condition, :blood_type)";
$stmt = $pdo->prepare($sql);
$stmt->bindValue('name', $name);
$stmt->bindValue('email', $email);
$stmt->bindValue('phone', $phone);
$stmt->bindValue('adress', $adress);
$stmt->bindValue('medical_condition', $medical_condition);
$stmt->bindValue('blood_type', $blood_type);

if ($stmt->execute()) {
    header("Location: list.php");
    die();
}

header("Location: index.php?status=error");
die();
