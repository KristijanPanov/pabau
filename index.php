<?php
session_start();

if (!isset($_SESSION['username'])) {
    header("Location: login.php");
    die();
}

require_once __DIR__ . "/db.php";
require_once __DIR__ . "/functions.php";
?>

<?php require_once __DIR__ . "/layout/header.php" ?>

<br />

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <form method="POST" action="store.php">
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" class="form-control" id="name" name="name">
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" id="email" name="email">
                </div>
                <div class="form-group">
                    <label for="phone">Phone</label>
                    <input type="number" class="form-control" id="phone" name="phone">
                </div>
                <div class="form-group">
                    <label for="adress">Adress</label>
                    <input type="text" min="1" max="100" class="form-control" id="adress" name="adress">
                </div>
                <div class="form-group">
                    <label for="medical_condition">Medical Condition</label>
                    <input type="text" min="1" max="100" class="form-control" id="medical_condition" name="medical_condition">
                </div>
                <div class="form-group">
                    <label for="blood_type">Blood Type</label>
                    <input type="text" min="1" max="100" class="form-control" id="blood_type" name="blood_type">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>
<?php require_once __DIR__ . "/layout/footer.php"; ?>