<?php

require_once __DIR__ . "/db.php";
require_once __DIR__ . "/functions.php";

$id = decrypt($_GET['id']);

$sql = "DELETE FROM users WHERE id = :id";

$stmt = $pdo->prepare($sql);
if ($stmt->execute(['id' => $id])) {
    header("Location: list.php");
    die();
}

echo "Can not delete user now";
